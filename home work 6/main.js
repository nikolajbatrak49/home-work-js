//* 1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування.
// Це заміна в тексті спеціальних(командних) символів на відповідні текстові вставки. Використовується у випадках 
// коли необхідно використовувати такі символи як звичайний символ в тексті.

//* 2.Які засоби оголошення функцій ви знаєте?
//Function declaration - Спочатку йде ключове слово function, після нього ім'я функції, потім список параметрів у круглих дужках через кому, потім код функції («тілом функції»), усередині фігурних дужок.
// Function expression - функція також оголошується за допомогою ключового слова function, але вона не має імені, і вона записується в змінну.
// Named Function expression - функція записується в змінну, а також має своє ім'я (правда функцію не можна буде викликати по цьому імені).

//* 3. Що таке hoisting, як він працює для змінних та функцій?
// Це - коли всі оголошення переміщуються у верхню частину поточної області (у верхню частину поточного скрипту чи поточної функції).
// Тобто змінні можуть бути ініціалізовані та використані до їх оголошення. Однак вони не можуть бути використані без ініціалізації. Змінні, визначені за допомогою let та const, піднімаються догори блоку, але не ініціалізуються.
// Оголошень функцій до виконання коду це можливість використовувати функцію до її оголошення.


function creatNewUser(
    firstName = prompt("What is your first name?"),
    lastName = prompt("What is your last name?"),
    birthday = prompt("enter your date of birth?")
) {
    return {
        firstName: firstName,
        lastName: lastName,
        birthday: birthday, 
        getAge: function () {
            yyyy = this.birthday.slice(6, 10);
            mm = this.birthday.slice(3, 5);
            dd = this.birthday.slice(0, 2);
            nowDate  = new Date();
            today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate());
            birthdayDate = new Date(yyyy, mm, dd);
            birthdayDateNow = new Date(today.getFullYear(), birthdayDate.getMonth(), birthdayDate.getDate()); //ДР в цьому році
            diffDate = today.getFullYear() - birthdayDate.getFullYear();
            if (today < birthdayDate) {
                diffDate = diffDate - 1;
            }
            return diffDate;
        },
        getPassword: function() {
            return userLogin = `${this.firstName[0].toUpperCase()}${this.lastName.toLowerCase()}${this.birthday.slice(6, 10)}`;
        }  
    }
}

let newUser = creatNewUser();

console.log(newUser);

console.log(newUser.getAge());

console.log(newUser.getPassword());