

//* Home work 14.....................................

// додаємо кнопку змінити тему

function addBtnChTh() {
  let imageWrapper = document.getElementsByClassName("images-wrapper");
  let btnChangeTheme = document.createElement('button'); 
  btnChangeTheme.className = "btnChangeTheme";
  btnChangeTheme.innerText = "Змінити тему";
  imageWrapper[0].after(btnChangeTheme);
  btnChangeTheme.setAttribute('onclick', 'changeTheme()');
}

let m = 0;
let themeNow = localStorage.getItem('keyTheme');

//функція зміни теми
function changeTheme() {
  m++;
  if (m % 2 !== 0) {
    document.body.style.background = 'red';
    localStorage.setItem('keyTheme', 'red');
  } else {
    document.body.style.background = '';
    localStorage.setItem('keyTheme', '');
  }
}

//перевірка на наявність збереженої теми в localStorage
function checkTheme() {
  if (themeNow === 'red') {
    document.body.style.background = 'red';
  } else {
    document.body.style.background = '';
  }
}

setTimeout(addBtnChTh, 500);
setTimeout(checkTheme, 1000);