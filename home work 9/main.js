//* Опишіть, як можна створити новий HTML тег на сторінці.
// Новий HTML тег можна створити за допомогою метода createElement().
// document.createElement(tag) --- де "document" - це елемент в якому створюється, а "tag" - це назва тегу що сворюється та прописується в кавички ""(Н-д: header, ul, li, a ... ).
//* Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
// Перший параметр - це спеціальне слово, що вказує, куди по відношенню до елементу(що вказується перед функцією та відокремлюється крапкою) робити вставку.
// `beforebegin' - вставити html безпосередньо перед елементом.
// `afterbegin' - вставити html на початок елементу.
// `beforeend' - вставити html в кінець елементу.
// `afterend' - вставити html безпосередньо після елементу.
//* Як можна видалити елемент зі сторінки?
// Для видалення вузла є метод remove(). Н-д (видалленя елементе з класом "className"):
// element = document.querySelector('.className');
// element.remove();


function arrayToList(array, parent = document.body) {
    let ul = document.createElement('ul');
    parent.append(ul);
    
    for (let index = 0; index < array.length; index++) {
        const element = array[index];
        let li = document.createElement('li');
        li.innerText = element;
        ul.insertAdjacentElement("afterbegin", li);
    }
}

arrayToList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);
arrayToList(["1", "2", "3", "sea", "user", 23]);
