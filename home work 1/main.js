// Як можна оголосити змінну у Javascript? var, const, let

// У чому різниця між функцією prompt та функцією confirm? confirm - отримує від користувача два значення true або false за допомогою button (ok/cancle). А prompt може прийняти будь-яке значення від користувача що буде введено ним в input вікна та натиснувши button (ok).

// Що таке неявне перетворення типів? Наведіть один приклад. 
// Це коли перетворення між різними типами може відбуватися автоматично, Найчастіше це відбувається коли ви застосовуєте оператори до значень різних типів, наприклад: 10 == null.

let name = "Mykola";
let admin;
admin = name;
console.log(admin);

let days = 10;
console.log(`${days*24*60*60} seconds`);

const answer = prompt("Write something");
console.log(answer);