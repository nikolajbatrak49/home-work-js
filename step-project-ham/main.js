//*Вкладки у секції Our services повинні перемикатися при натисканні мишею. Текст та картинки для інших вкладок додати будь-які.

// додамо до дочірніх елементів елемента "tabs" атрибудти-ідентифікатори 'data-tab'
function setData() {
    let tab = document.querySelectorAll(".services-tabs>li");
    tab.forEach(element => {
        child = element.querySelector('.services-tab-item-text');
        if (!element.hasAttribute('data-tab')) {
            element.setAttribute('data-tab', `${child.textContent}`);
        } 
        element.setAttribute('onclick', `clickTab("${element.getAttribute('data-tab')}"); getContent("${element.getAttribute('data-tab')}");`)
    });
}

// функція-маркер, що позначає котрий саме обраний елемент з класом "services-tab-item" 
function clickTab(tabTitle) {  
    let tab = document.querySelectorAll(".services-tabs>li");
    tab.forEach(element => {
        if (element.getAttribute('data-tab') === tabTitle) {
            element.setAttribute('class', 'services-tab-item selected');
        } else {
            element.setAttribute('class', 'services-tab-item');
        }
    });
}

// функція виводу контенту згідно натиснутого елементу з класом "services-tab-item"
function getContent(tabTitle) {
    let content = document.querySelectorAll('.services-contents>li');
    content.forEach(element => {
        child = element.querySelector('.services-content-description');
        if (child.textContent.indexOf(tabTitle) !== -1) {
            element.style.display = "";
        } else {
            element.style.display = "none";
        }
    });
}

setData();

if (document.getElementsByClassName('services-tab-item selected')) {
    getContent(document.getElementsByClassName('services-tab-item selected')[0].getAttribute('data-tab'));
}

//* Кнопка Load more у секції Our amazing work імітує завантаження з сервера нових картинок. При її натисканні в секції знизу мають з'явитись ще 12 картинок (зображення можна взяти будь-які). Після цього кнопка зникає.


let btnLoadMore = document.getElementById('loadMore');

//встановлення функції додавання карток по кліку на кнопку
btnLoadMore.addEventListener('click', () => {
    btnLoadMore.style.display = 'none';
    newContents('./public/image/landing page/landing-page1.jpg', 'Graphic Design');
    newContents('./public/image/landing page/landing-page2.jpg', 'Graphic Design');
    newContents('./public/image/landing page/landing-page3.jpg', 'Graphic Design');
    newContents('./public/image/landing page/landing-page4.jpg', 'Web Design');
    newContents('./public/image/landing page/landing-page5.jpg', 'Web Design');
    newContents('./public/image/landing page/landing-page6.jpg', 'Web Design');
    newContents('./public/image/wordpress/wordpress1.jpg', 'Landing Pages');
    newContents('./public/image/wordpress/wordpress2.jpg', 'Landing Pages');
    newContents('./public/image/wordpress/wordpress3.jpg', 'Landing Pages');
    newContents('./public/image/wordpress/wordpress4.jpg', 'Wordpress');
    newContents('./public/image/wordpress/wordpress5.jpg', 'Wordpress');
    newContents('./public/image/wordpress/wordpress6.jpg', 'Wordpress');
    checkTab();
});

//функція створення картки
function newContents(addressImg, tab) {
    let contents = document.querySelector('.work-contents');
    let content = document.createElement('li');
    content.className = "work-content-list";
    content.innerHTML = `
    <img class="work-content-img" src="${addressImg}">
    <div class="work-content-list__inner">
      <div class="work-content-icon__wrapper">
        <div class="work-content-icon-frame">
          <svg class="work-content-icon" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path fill-rule="evenodd" clip-rule="evenodd" d="M13.9131 2.72817L12.0948 0.891285C11.2902 0.0808612 9.98305 0.0759142 9.17681 0.882615L7.15921 2.89256C6.35161 3.69885 6.34818 5.01032 7.15051 5.82074L8.30352 4.68897C8.18678 4.32836 8.33041 3.9153 8.61593 3.62946L9.89949 2.35187C10.3061 1.94624 10.9584 1.94913 11.3595 2.35434L12.4513 3.45805C12.8528 3.86283 12.8511 4.51713 12.447 4.92318L11.1634 6.20241C10.8918 6.47296 10.4461 6.62168 10.1002 6.52626L8.97094 7.65887C9.77453 8.47177 11.0803 8.47466 11.8889 7.66837L13.9039 5.65924C14.7141 4.85254 14.7167 3.53983 13.9131 2.72817ZM6.52613 10.0918C6.62191 10.4441 6.46857 10.8997 6.19093 11.1777L4.99227 12.3752C4.58074 12.7845 3.91595 12.7833 3.50671 12.369L2.39297 11.2475C1.98465 10.8349 1.98729 10.1633 2.39824 9.75473L3.59804 8.55769C3.89032 8.26607 4.31044 8.12025 4.67711 8.24375L5.83354 7.0715C5.01493 6.2462 3.68249 6.24207 2.86059 7.06324L0.915197 9.0042C0.0922615 9.8266 0.0883685 11.1629 0.90651 11.9886L2.75817 13.8618C3.57595 14.6846 4.90724 14.6912 5.73111 13.8701L7.67649 11.9287C8.49852 11.1054 8.5024 9.77166 7.68553 8.9443L6.52613 10.0918ZM6.25787 9.56307C5.98013 9.84189 5.53427 9.84105 5.26179 9.55812C4.98792 9.27434 4.9901 8.82039 5.26613 8.53993L8.59075 5.16109C8.86679 4.88227 9.31174 4.88311 9.58513 5.16398C9.86048 5.44569 9.85876 5.90088 9.5817 6.18299L6.25787 9.56307Z" fill="#1FDAB5"/>
          </svg>
        </div>
        <div class="work-content-icon-frame">
          <svg class="work-content-icon" width="12" height="11" viewBox="0 0 12 11" fill="none" xmlns="http://www.w3.org/2000/svg">
          <rect width="12" height="11" fill="#1FDAB5"/>
          </svg>                      
        </div>
     </div>
      <h3 class="work-content-title">creative design</h3>
      <p class="work-content-description">
        ${tab}
      </p>
    </div>
    `;
    contents.append(content);    
}

//* Кнопки на вкладці Our amazing work є "фільтрами продукції". Попередньо кожній із картинок потрібно присвоїти одну з чотирьох категорій, на ваш розсуд (на макеті це Graphic design, Web design, Landing pages, Wordpress).При натисканні на кнопку категорії необхідно показати лише ті картинки, які належать до цієї категорії. All показує картинки з усіх категорій. Категорії можна перейменувати, картинки для категорій взяти будь-які.

//встановлення виклику функцій по кліку на таб з сеції ворк
function setClick() {
    let tab = document.querySelectorAll(".work-tabs>li");
    tab.forEach(element => {
        element.setAttribute('onclick', `activeContent("${element.textContent}");`)
    });
}
setClick();

// функція сортування карток згідно їх контенту
function activeContent(tabTitle) {  
    let content = document.querySelectorAll(".work-content-list");
    activeTab(tabTitle);
    showAll();
    content.forEach(element => {
        child = element.querySelector('.work-content-description');
        if (tabTitle === 'All' || tabTitle === child.innerText){
            element.style.display = '';
        } else {
            element.style.display = 'none';
        }
    });
}
//допоміжна функція - оновлення карток що завантажено
function showAll() {
    let content = document.querySelectorAll(".work-content-list");
    content.forEach(element => {
        element.style.display = '';
    });
}

// функція-маркер, що позначає активний таб
function activeTab(tabTitle) {  
    let tab = document.querySelectorAll(".work-tabs>li");
    tab.forEach(element => {
        if (tabTitle === element.innerText) {
            element.setAttribute('class', 'work-tab-item selected');
        } else {
            element.setAttribute('class', 'work-tab-item');
        }
    });
}

// функція перевірки активного табу після натискання кнопки btnLoadMore
function checkTab() {  
    let tab = document.querySelectorAll(".work-tabs>li");
    tab.forEach(element => {
        if (element.getAttribute('class')==='work-tab-item selected') {
            activeContent(element.innerText);            
        }
    });
}

//*Карусель на вкладці What people say about theHam має бути робочою, по кліку як на іконку фотографії внизу, так і на стрілки вправо-вліво. У каруселі має змінюватися як картинка, і текст. Карусель обов'язково має бути з анімацією.

let btnBack = document.getElementsByClassName('button-back');
let btnNext = document.getElementsByClassName('button-next');
let users = document.querySelectorAll('.user-list>li');
let selectImgUser = document.getElementsByClassName('user-icon-selected');
let comments = document.querySelectorAll('.comments>li');

let i=0;
//функція кнопки перемикання назад на каруселі
function stepBack() {
    i--;
    if (i < 0) {
        i = users.length - 1;
    }
    showUserActive(i);
}
btnBack[0].addEventListener('click', stepBack);

//функція кнопки перемикання вперел на каруселі
function stepForward() {
    i++;
    if (i >= users.length) {
        i = 0;
    }
    showUserActive(i);
}
btnNext[0].addEventListener('click', stepForward);

//функція відображення обраного юзера в каруселі 
function showUserActive(n) {
    let p = 0;
    users.forEach(element => {
        child = element.querySelector('.user-item-icon');
        atrChild = child.getAttribute('src');
        person = element.getAttribute('data-person');
        if (n == p) {
            element.setAttribute('class', 'user-item selected');
            selectImgUser[0].setAttribute('src', atrChild);
            u = 0;
            animate();
            showComment(person);
        } else {
            element.setAttribute('class', 'user-item');
        }
        p++;
    });
}

//функція-анімація - плавне відображення
let u = 0;
let timeout;

function animate() {
    u++; 
    timeout = setTimeout(animate, 120);
    selectImgUser[0].style.opacity = u*0.1;
    document.getElementsByClassName('comments')[0].style.opacity = u*0.1;
    if (u*0.1 == 1) {
        clearTimeout(timeout);
    }
}

//функція відображення контенту обраного юзера в каруселі 
function showComment(userName) {
    comments.forEach(element => {
        child = element.querySelector('.user-name');
        if (userName === child.innerText){
            element.style.display = '';
        } else {
            element.style.display = 'none';
        }
    });
}