let inputPassword = document.querySelectorAll('input');
let show = document.querySelectorAll("i");

//функція налаштування кліку на кнопку та іконки
function setClick() {
    let i = 0;
    let button = document.querySelector('.btn');
    show.forEach(element => {
        if (!element.hasAttribute('onclick')) {
            element.setAttribute('onclick', `showPassword(${i}), `);
        } 
        i++;
    });
    button.setAttribute('onclick', 'passwordСomparison()');
    noReboot();
}

//функція щоб не перезавантажувалась сторінка
function noReboot() {
    let form = document.querySelector(".password-form");
    if (!form.hasAttribute('onsubmit')) {
        form.setAttribute('onsubmit', 'return false');
    }
}

//функція показування паролей
function showPassword(i) {
    if (inputPassword[i].type === "password") {
    inputPassword[i].type = "text";
    show[i].className = 'fas fa-eye-slash icon-password';
    } else {
        inputPassword[i].type = "password";
        show[i].className = 'fas fa-eye icon-password';
    }
}

//функція порівняння паролей між інпутами
function passwordСomparison() {
    if (inputPassword[0].value === inputPassword[1].value) {
        alert('You are welcome');
    } else {
        let div = document.createElement('div');
        div.className = "error";
        div.innerText = "Потрібно ввести однакові значення";
        div.style.color = "red";
        inputPassword[1].after(div);
    }
}

setClick();