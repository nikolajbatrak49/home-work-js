// 1. Опишіть своїми словами, що таке метод об'єкту
// Це функція, що додана у ролі властивості об'єкту.

// 2. Який тип даних може мати значення властивості об'єкта?
// Це може бути: рядок, число, булевий тип (true/false), null, або інший об'єкт.

// 3. Об'єкт це посилальний тип даних. Що означає це поняття?
// Це значення в пам'яті, на яке можна послатися за допомогою ідентифікатора(змінної). Тобто змінні не зберігають в собі самі об'єкти, а тільки посилаються на них.




function creatNewUser() {
    this.firstName = prompt("What is your first name?");
    this.lastName = prompt("What is your last name?");
    this.getLogin = function() {
        let userLogin = `${this.firstName[0]}${this.lastName}`.toLowerCase();
        return userLogin;
    }
}

let newUser = new creatNewUser();

console.log(newUser.getLogin());



// Object.defineProperty(newUser, 'firstName', {
//     set(newValue) {
//         firstName = newValue;
//     }
// });

// Object.defineProperty(this, 'lastName', {
//     set: function(value) {
//         lastName = value;
//     }
// });