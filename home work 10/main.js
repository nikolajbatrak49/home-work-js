// додамо до дочірніх елементів елемента "tabs" атрибудти-ідентифікатори 'data-tab'
function setData() {
    let tab = document.querySelectorAll(".tabs>li");
    tab.forEach(element => {
        if (!element.hasAttribute('data-tab')) {
            element.setAttribute('data-tab', `${element.textContent}`);
        } 
        element.setAttribute('onclick', `clickTab("${element.getAttribute('data-tab')}"); getContent("${element.getAttribute('data-tab')}");`)
    });
}

// функція-маркер, що позначає котрий саме обраний елемент з класом "tabs-title" 
function clickTab(tabTitle) {  
    let tab = document.querySelectorAll(".tabs>li");
    tab.forEach(element => {
        if (element.getAttribute('data-tab') === tabTitle) {
            element.setAttribute('class', 'tabs-title active');
        } else {
            element.setAttribute('class', 'tabs-title');
        }
    });
}

// функція виводу контенту згідно натиснутого елементу з класом "tabs-title"
function getContent(tabTitle) {
    let content = document.querySelectorAll('.tabs-content>li');
    content.forEach(element => {
        if (element.textContent.indexOf(tabTitle) !== -1) {
            element.style.display = "";
        } else {
            element.style.display = "none";
        }
    });
}


setData();

if (document.getElementsByClassName('tabs-title active')) {
    getContent(document.getElementsByClassName('tabs-title active')[0].getAttribute('data-tab'));
}
