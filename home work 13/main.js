//* 1. Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.
// setTimeout викликає функцію один раз, а setInterval регулярно. Рекурсивний setTimeout виконує фіксовану затримку після виконання попередньої функції перед викликом наступної, а setInterval встановлює затримку від початку виконання попередньої функції. Іноді час виконання такої функції може бути більший за сам інтервал і тоді наступна функція не буде викликана поки не завершиться попередня.
//* 2. Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
// Тоді відбудеться виклик функції настільки швидко, наскільки це можливо. Але setTimeout викликатиме функцію лише після завершення виконання поточного коду, якщо такий є.
//* 3. Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?
//По-переше щоб зупинити сам планувальник, а по-друге так як викликана функція буде лишатися в пам'яті планувальника доти поки не буде викликано clearInterval. 



let i = 0;
let timeout;
let imageWrapper = document.getElementsByClassName("images-wrapper");

// функція встановлення кнопок
function addBtn() {
  let btnStop = document.createElement('button'); 
  let btnPlay = document.createElement('button');  
  btnStop.className = "btnStop";
  btnStop.innerText = "Припинити";
  btnPlay.className = "btnPlay";
  btnPlay.innerText = "Відновити показ";
  imageWrapper[0].after(btnStop);
  imageWrapper[0].after(btnPlay);
  btnStop.setAttribute('onclick', 'stopChangeImg()');
  btnPlay.setAttribute('onclick', 'startChangeImg()');
}

// функція запуску показу картинок
function startChangeImg() {
    changeImg();
}

// функція зупинки показу картинок
function stopChangeImg() {
  clearTimeout(timeout);
}

// функція показу картинки з індексом n на екрані
function showImg(n) {
  let image = document.querySelectorAll('.image-to-show');
  let p = 0;
  image.forEach(element => {
      if (n == p) {
          element.style.display = '';
      } else {
          element.style.display = 'none';
      }
      p++;
  });
}

//функція показу картинок з встановленим інтервалом часу 3 сек.
function changeImg() {
    let image = document.getElementsByClassName('image-to-show');
    i++;
    if (i >= image.length) {
        i = 0;
    }
    timeout = setTimeout(changeImg, 3000);
    showImg(i);
}

//запуск програми
changeImg();
setTimeout(addBtn, 2000);