//* Опишіть своїми словами що таке Document Object Model (DOM)
// Об'єктна модель документа - це не залежний від платформи та мови інтерфейс, який дозволяє програмам та сценаріям динамічно отримувати доступ та оновлювати вміст, структуру та стиль документа.
//* Яка різниця між властивостями HTML-елементів innerHTML та innerText?
// Властивість innerHTML дозволяє отримати HTML-вміст елемента у вигляді рядка.
// Ми також можемо змінювати його. Це один із способів міняти вміст на сторінці.
// innerText дозволяє читати або задавати текстовий вміст елемента. При зчитуванні тексту з елемента буде повернуто рядок із текстовим вмістом усіх вкладених дочірніх елементів.

// Тобто innerHTML дозволяє змінювати/отримувати HTML-вміст елемента(в цілому), а innerText тільки текстовий вміст.  
//* Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
// Якщо елемент має атрибут id, то ми можемо звернутися до нього викликом document.getElementById(id), де б він не знаходився.
// Найуніверсальніший метод – це elem.querySelectorAll(css), він повертає всі елементи всередині elem, що задовольняють даному CSS-селектору.
// Який метод краще важко сказати, мабуть більш залежить від конкретного випадку. Але, частіше застосовується getElementById. 


// 1. Знайти всі параграфи на сторінці та встановити колір фону #ff0000

let elements = document.querySelectorAll('p');
for (let elem of elements) {
    (elem.style.backgroundColor = '#ff0000'); 
  }

// 2. Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

let elementS = document.getElementById('optionsList');
console.log(elementS);

let parElem = elementS.parentNode;
console.log(parElem);

if(elementS.hasChildNodes()){
for (let i = 0; i < elementS.childNodes.length; i++) {
    console.log(elementS.childNodes[i].nodeName, elementS.childNodes[i].nodeType);
  }
}

//   3. Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph

let content = document.getElementById('testParagraph');

content.innerText = 'This is a paragraph';

// 4. Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.

let elemWithClass = document.querySelector('.main-header');

for (let i = 0; i < elemWithClass.childNodes.length; i++) {
    console.log(elemWithClass.childNodes[i]);
    console.log(elemWithClass.childNodes[i].className = "nav-item");
  }

// 5. Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

let elementsWithClass = document.querySelectorAll('.section-title');

for (let elem of elementsWithClass) {
    elem.classList.remove('section-title'); 
  }