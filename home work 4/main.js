// 1. Описати своїми словами навіщо потрібні функції у програмуванні.
// Функції потрібні щоб не повторювати той самий код та ту саму дію у багатьох місцях програми. 

// 2. Описати своїми словами, навіщо у функцію передавати аргумент.
// Аргумент – це значення, яке передається функції під час її виклику в якості її параметру. Бо якщо не передати аргумент функції то параметр функції буде === undefined.

// 3. Що таке оператор return та як він працює всередині функції?
// Це оператор, що завершує виконання поточної функції та повертає її значення. 
// Оператор return може бути в будь-якому місці всередині функції. Як тільки виконання доходить до цього місця, функція зупиняється, і значення повертається в код, що її викликав.
// Викликів return може бути декілька всередині функції.


// firstNumber != ""

let firstNumber;
let secondNumber;
let action;

while (true) {
  firstNumber = Number(prompt("Please enter first number:", firstNumber));
  if (firstNumber != "" || !typeof firstNumber === "string") { 
    secondNumber = prompt("Please enter second number:", secondNumber);
    if (secondNumber != "" || !typeof secondNumber === "string") {
      action = prompt ("What math calculation do you want to do?(+, -, *, /)", action);
      break;
    }
  }
}

function calc(a, b, c) {
  if (c === "+") {
    return +a + +b;
  }
  else if (c === "-") {
    return +a - +b;
  }
  else if (c === "*") {
    return +a * +b;
  }
  else if (c === "/") {
    return +a / +b;
  }
}

result = calc(firstNumber, secondNumber, action);
console.log(result);