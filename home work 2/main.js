// 1. Які існують типи даних у Javascript?
// - String - строка
// - Number - числа
// - Boolean - правильне або хибне значення
// - null - значення невідоме, або відсутнє
// - undefined - значення не було присвоєне

// 2. У чому різниця між == і ===?
// == - це рівність по значенню;
// === - це рівність по значенню та по типу.

// 3. Що таке оператор?
// Це елемент мови, що визначає повний опис дій, які необхідно виконати.

let name  
let age

while (true) {
  name = prompt("What is you name?", name);
  if (name != "") { 
    age = prompt("How old are you?", age);
    if (age > 0 || !typeof age === "string") {
      break;
    }
  }
}

if (age<18) {
    alert('You are not allowed to visit this website.');
} else if (age>=18 && age<=22) {
    let questionVisit = confirm('Are you sure you want to continue?');
    if (questionVisit) {
        alert (`Welcome, ${name}`);
    } else {
        alert('You are not allowed to visit this website.');
    }
} else if (age>22) {
    alert (`Welcome, ${name}`);
}
